import { BrowserWindow, screen } from 'electron';

export default class MainWindow {

    public constructor(targetPath: string, opts: string, display: Electron.Display, fullscreen: boolean = true) {
        this.window = new BrowserWindow({
            webPreferences: {
                nodeIntegration: false,
                preload: __dirname + "/../scripts/preload.js"
            },
            autoHideMenuBar: true,
            x: display.bounds.x,
            y: display.bounds.y,
            fullscreen: fullscreen,
            frame: false
        });

        this.window.on('close', this.destroy);
        this.window.webContents.executeJavaScript(opts, true);
        this.window.loadURL(targetPath);
        this.window.setMenu(null);
        this.window.setAlwaysOnTop(true);
    }

    public getElectronWindow() {
        return this.window;
    }

    protected destroy() {
        this.window = null;
    }

    private window: Electron.BrowserWindow | null;
}