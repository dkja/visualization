import { app, remote, screen, globalShortcut, dialog } from 'electron'
import MainWindow from './MainWindow'

export default class Application {
    public static run() {
        app.on('window-all-closed', Application.onWindowAllClosed);
        app.on('ready', () => Application.main());

    }

    protected static main() {

        // Ctrl+W - EXIT
        globalShortcut.register("Escape", app.quit);

        // display detection
        let displays = screen.getAllDisplays();
        process.stdout.write(`[INFO]\t Detected displays: ${displays.length} \n`);

        const DISPLAY_NUMBER = 2;

        if (displays.length < DISPLAY_NUMBER) {
            dialog.showErrorBox("Too few displays", `This App requires at least ${DISPLAY_NUMBER} displays. Detected:  ${displays.length}`);
            process.abort();
        }


        // second window is on two displays.

        /// CONFIGS:
        const url = 'https://develop-federal.seth.pl';

        const display1Nr = 0;
        const display2Nr = 1;

        const redirect = 'visualization';


        const loginData = ' userName: "visualization",    \
                            userPass: "alsdufhaewfa83" ';

        const opt = ' var opts; window.opts = {' + loginData + ', redirectTo: "' + redirect + '"}; ';





        // make windows:
        Application.window = new MainWindow(url, opt, displays[display1Nr], false);
        var native = Application.window.getElectronWindow();


        // SET DOUBLE SIZE for second screen
        var height = (displays[display1Nr].size.height);
        var width = (displays[display1Nr].size.width) + (displays[display2Nr].size.width);

        if (
            (displays[display1Nr].size.height != displays[display2Nr].size.height) ||
            (displays[display1Nr].size.width != displays[display2Nr].size.width)
        )

            process.stdout.write(`[WARN]\t Displays size is not equal\n`);


        native.setMaximumSize(width, height);
        native.setMinimumSize(width, height);
        native.setSize(width, height);


    }


    protected static onWindowAllClosed() {
        if (process.platform !== 'darwin')
            app.quit();
    }

    private static window: MainWindow;
}